# Corpus Carolina

Carolina is a general corpus of contemporary Brazilian Portuguese _with provenance and typology information_.

Carolina is composed of texts gathered in several digital repositories, whose licenses are multiple and, therefore, must be strictly observed when using the corpus.

The specific licenses for each document included in the Corpus are detailed in its metadata. They range from broad public domain licenses to partial sharing licenses with restrictions on commercial use. No unlicensed documents were included in the Corpus.

The Corpus header is under the following sharing license:
Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License

Here you can find current and past versions of the Carolina Corpus for download. 

Current version is **1.0-Ada**. This version is made available without any programming support.  Programming support is planned for future versions.

More information can be found [here](https://sites.usp.br/corpuscarolina/). 
